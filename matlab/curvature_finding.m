%% MATLAB script to find sharp curvature radius
%% Author: Alejandro Rodríguez-Ramos
%% Date: 3 de Agosto de 2016
%% Email: alejandro.dosr@gmail.com

% Clean screen
clc
% clear

% Parameters
window_size = 50;
dist_bet_max = 20;
dist_bet_points = 50;
N_lines = 2;
angle_max = 130;
d_from_anchor = 1.0;

% Parabollic function
% y = [-12:0.01:12];
% x = 2*sin(y);
% plot(x, y)
% hold

% Smooth a little the trajectory
% x_soft = tsmovavg(x, 's', 10, 1);
% y_soft = tsmovavg(y, 's', 10, 1);
x_soft = sma(x, 10);
y_soft = sma(y, 10);
% plot(x_soft, y_soft)
% hold

% Extract middle points of the sharp-curves
% avg_x = tsmovavg(abs(diff(diff(x_soft))), 's', window_size, 1);
% avg_y = tsmovavg(abs(diff(diff(y_soft))), 's', window_size, 1);
avg_x = sma(abs(diff(diff(x_soft))), window_size);
avg_y = sma(abs(diff(diff(y_soft))), window_size);
figure
plot(avg_x)

zero_slope_i_y = find(diff(avg_y) > 0);
zero_slope_i_y = zero_slope_i_y(find(diff(zero_slope_i_y) > dist_bet_max)) - ceil(window_size / 2);
zero_slope_i_x = find(diff(avg_x) > 0);
zero_slope_i_x = zero_slope_i_x(find(diff(zero_slope_i_x) > dist_bet_max)) - ceil(window_size / 2);

zero_slope_i = [zero_slope_i_x' zero_slope_i_y'];

zero_slope_i = zero_slope_i(find(zero_slope_i > 0));

figure
plot(x, y)
hold

for j = 1:length(zero_slope_i)   
    % Finding the slope to the line tangent to the sharp-curve
    x1 = x(zero_slope_i(j));
    x2 = x (zero_slope_i(j) + 1);
    y1 = y(zero_slope_i(j));
    y2 = y(zero_slope_i(j) + 1);
    m_zero = (y2 - y1) / (x2 - x1);
    o_zero = y1 - m_zero * x1;
    y_zero = m_zero * x + o_zero;
    
    % Plot the point and the line
    y_zero_modified = y_zero;
    y_zero_modified(1:zero_slope_i(j) - window_size) = NaN;
    y_zero_modified(zero_slope_i(j) + window_size:end) = NaN;
    plot(x, y_zero_modified, '--')
    plot(x(zero_slope_i), y(zero_slope_i), '*r')

    % Loop to find lines and angles    
    intersec_avg_r = [];
    intersec_avg_l = [];
    for i = 1 : N_lines
        % Find the line
        if zero_slope_i(j) - i * dist_bet_points >= 1
            x1 = x(zero_slope_i(j) - i * dist_bet_points);  
            x2 = x (zero_slope_i(j) - i * dist_bet_points + 1);
            y1 = y(zero_slope_i(j) - i * dist_bet_points);
            y2 = y(zero_slope_i(j) - i * dist_bet_points + 1);
            m = (y2 - y1) / (x2 - x1);
            o = y1 - m * x1;
            y_tangent = m * x + o;           

            % Find the intersection point to the y_zero
            intersec_i = find(abs(y_tangent - y_zero) == min(abs(y_tangent - y_zero)));  
            if length(intersec_i) > 0
                if(intersec_i(1) ~= length(x))
                    % Find the angle
                    x1 = x(intersec_i(1) + 1) - x(intersec_i(1));
                    x2 = x(intersec_i(1) + 1) - x(intersec_i(1));
                    y1 = y_zero(intersec_i(1) + 1) - y_zero(intersec_i(1));
                    y2 = y_tangent(intersec_i(1) + 1) - y_tangent(intersec_i(1));
                    dot = x1*x2 + y1*y2;
                    det = x1*y2 - y1*x2;
                    angle = 180 - abs(atan2(det,dot) * 180 / pi)

                    % Plot the point and the line
                    y_tangent(1:zero_slope_i(j) - i * dist_bet_points - window_size) = NaN;
                    y_tangent(zero_slope_i(j) - i * dist_bet_points + window_size:end) = NaN;
                    plot(x, y_tangent, ':')

                    % Plot the points of the angle
                    if angle < angle_max                    
    %                     plot(x(intersec_i(1)), y_zero(intersec_i(1)), 'xm')
    %                     intersec_avg_r = [intersec_avg_r intersec_i(1)]; 
%                         if zero_slope_i(j)+separation_from_anchor < length(x)
                            intersec_avg_r = [intersec_avg_r 1];
%                         end
%                         if zero_slope_i(j)-separation_from_anchor > 0
                            intersec_avg_l = [intersec_avg_l 1];
%                         end
            %             plot(x(intersec_i(1) + 1), y_zero(intersec_i(1) + 1), '+')
            %             plot(x(intersec_i(1) + 1), y_tangent(intersec_i(1) + 1), '+')
                    end
                end
            end
        end
        
        if zero_slope_i(j) + i * dist_bet_points < length(y)
            x1 = x(zero_slope_i(j) + i * dist_bet_points);  
            x2 = x (zero_slope_i(j) + i * dist_bet_points + 1);
            y1 = y(zero_slope_i(j) + i * dist_bet_points);
            y2 = y(zero_slope_i(j) + i * dist_bet_points + 1);
            m = (y2 - y1) / (x2 - x1);
            o = y1 - m * x1;
            y_tangent = m * x + o;           

            % Find the intersection point to the y_zero
            intersec_i = find(abs(y_tangent - y_zero) == min(abs(y_tangent - y_zero)));            
            
            if length(intersec_i) > 0
                if(intersec_i(1) ~= length(x))
                    % Find the angle
                    x1 = x(intersec_i(1) + 1) - x(intersec_i(1));
                    x2 = x(intersec_i(1) + 1) - x(intersec_i(1));
                    y1 = y_zero(intersec_i(1) + 1) - y_zero(intersec_i(1));
                    y2 = y_tangent(intersec_i(1) + 1) - y_tangent(intersec_i(1));
                    dot = x1*x2 + y1*y2;
                    det = x1*y2 - y1*x2;
                    angle = 180 - abs(atan2(det,dot) * 180 / pi)

                    % Plot the point and the line
                    y_tangent(1:zero_slope_i(j) + i * dist_bet_points - window_size) = NaN;
                    y_tangent(zero_slope_i(j) + i * dist_bet_points + window_size:end) = NaN;
                    plot(x, y_tangent, ':')

                    % Plot the points of the angle
                    if angle < angle_max
    %                     plot(x(intersec_i(1)), y_zero(intersec_i(1)), 'xc')
    %                     intersec_avg_l = [intersec_avg_l intersec_i(1)];
%                           if zero_slope_i(j)-separation_from_anchor > 0
                            intersec_avg_l = [intersec_avg_l 1];
%                           end
%                           if zero_slope_i(j)+separation_from_anchor < length(x)
                            intersec_avg_r = [intersec_avg_r 1];
%                           end
            %             plot(x(intersec_i(1) + 1), y_zero(intersec_i(1) + 1), '+')
            %             plot(x(intersec_i(1) + 1), y_tangent(intersec_i(1) + 1), '+')    
                    end
                end
            end
        end
    end
    
    % Plot the intersect    
    if length(intersec_avg_r > 0)
        intersec_avg_r = floor(sum(intersec_avg_r)/length(intersec_avg_r));
%         plot(x(intersec_avg_r), y_zero(intersec_avg_r), 'xc')
        x_plotted = x(zero_slope_i(j)) + d_from_anchor / sqrt(m_zero^2 + 1);
        y_plotted = x_plotted  * m_zero + o_zero;
        plot(x_plotted, y_plotted, 'xc')
        % Plot the closest point
        closest_i = find(sqrt((x_plotted - x).^2 + (y_plotted - y).^2) == ...
                                    min(sqrt((x_plotted - x).^2 + (y_plotted - y).^2)));
        plot(x(closest_i), y(closest_i), 'xb')
        % Plot a possible surrogate
        plot(x_plotted / 2 + x(closest_i) / 2, y_plotted / 2 + y(closest_i) / 2, 'xg')
    end
    if length(intersec_avg_l > 0)
        intersec_avg_l = floor(sum(intersec_avg_l)/length(intersec_avg_l));
%         plot(x(intersec_avg_l), y_zero(intersec_avg_l), 'xc')
         x_plotted = x(zero_slope_i(j)) - d_from_anchor / sqrt(m_zero^2 + 1);
        y_plotted = x_plotted  * m_zero + o_zero;
        plot(x_plotted, y_plotted, 'xc')
        % Plot the closest point
%         closest_i = find(sqrt((x(intersec_avg_l) - x).^2 + (y_zero(intersec_avg_l) - y).^2) == ...
%                                     min(sqrt((x(intersec_avg_l) - x).^2 + (y_zero(intersec_avg_l) - y).^2)));
        closest_i = find(sqrt((x_plotted - x).^2 + (y_plotted - y).^2) == ...
                                    min(sqrt((x_plotted - x).^2 + (y_plotted - y).^2)));
        plot(x(closest_i), y(closest_i), 'xb')
        % Plot a possible surrogate
        plot(x_plotted/2 + x(closest_i)/2,y_plotted/2 + y(closest_i)/2, 'xg')
    end
end
