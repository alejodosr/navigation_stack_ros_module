//////////////////////////////////////////////////////
///  DroneNavigationStackROSModule.h
///
///  Created on: May 17, 2016
///      Author: alejodosr
///
///  Last modification on: Aug 11, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////


//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3Stamped.h>
#include<geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>


//Ground Robots: Messages out
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneSpeeds.h>

#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <nav_msgs/Odometry.h>
#include "geometry_msgs/PoseStamped.h"

#include "tf/transform_datatypes.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"

#include "sensor_msgs/LaserScan.h"
#include <fstream>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/chrono.hpp>

#include <algorithm>


//#include <droneMsgsROS/vectorTargetsInImageStamped.h>
//#ifndef DEBUG_MODE
//    #define DEBUG_MODE
//#endif

#define M_PI                3.14159265358979323846
#define M_HALF_PI           3.14159265358979323846/2
#define M_THREE_HALF_PI     3*3.14159265358979323846/2
#define FORTY_FIVE_IN_RAD   3.14159265358979323846/4
#define MIN_DIFF            0.2
//#define X_TOLERANCE         0.2
//#define Y_TOLERANCE         0.2
#define ROT_TOLERANCE       M_PI/6
#define ALTITUDE            1.3
//#define FOLLOWING_INDEX     6
#define YAW_TIMEOUT_SEC     5

#define FAR_DIST_TO_OBS     5
#define MEDIUM_DIST_TO_OBS  1.4
#define CLOSE_DIST_TO_OBS   0.8
#define X_TOLERANCE_CLOSE   0.4
#define Y_TOLERANCE_CLOSE   0.4
#define X_TOLERANCE_MEDIUM  0.15
#define Y_TOLERANCE_MEDIUM  0.15
#define X_TOLERANCE_FAR     0.5
#define Y_TOLERANCE_FAR     0.5

#define DIST_BET_POINTS_0   0.7
#define DIST_BET_POINTS_1   0.4
#define DIST_BET_POINTS_2   0.1

// Status WRT obstacles
#define STATUS_FAR          0x0
#define STATUS_MEDIUM       0x1
#define STATUS_CLOSE        0x2

// Sharp curve correcting
#define WINDOW_SIZE             50
#define DIST_BET_MAX            20
#define DIST_BET_INFLECTION     40
#define N_LINES                 2
#define ANGLE_MAX               130
#define MOVE_OBS_MAX            5
#define D_FROM_ANCHOR           1.0
#define SUBGOALS_TOL            0.5
//#define SEPARATION_FROM_ANCHOR  30

// Matlab-like find
#define LOWER               0x0
#define LOWER_EQUAL         0x1
#define EQUAL               0x2
#define HIGHER_EQUAL        0x3
#define HIGHER              0x4

#define TOL_IN_LAST_GOAL    0.2
#define TOL_IN_LAST_GOAL_Z  0.3
#define NO_GOAL_REACHED			5 		

struct traj_points{
    double x;
    double y;
		double z;
    double x_closest;
    double y_closest;
    int index;
};

struct line{
    double m;
    double o;
};

/////////////////////////////////////////
// Class DroneNavigationStackROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneNavigationStackROSModule : public DroneModule
{
public:

    /// Position controller members
    ///
    geometry_msgs::Vector3Stamped rotation_angles;
    droneMsgsROS::droneYawRefCommand Yaw;
    geometry_msgs::PoseStamped current_pose, previous_pose, following_pose;
    nav_msgs::Odometry odom_pose;
    int current_index, following_index, following_index_0, following_index_1, following_index_2;
    nav_msgs::Path current_trajectory;
    bool trajectory_set, dont_move, dont_yaw;
    double previous_angle, current_yaw, x_tolerance, y_tolerance, last_goal_z;
    int status;
    ros::Time timeout;
    std::vector<double> ranges;
    std::ofstream File;
    int traj_num, n_obs_counter;
    std::vector< std::vector< traj_points > > subgoals;
    // NavigationStack ROS Module state variables
    bool subtrajectory, goal_received, was_me, final_goal;
    traj_points last_sub_goal_sent, last_goal_sent;
    boost::mutex subtrajectory_mutex, goal_received_mutex, was_me_mutex,
                    subgoals_mutex, last_sub_goal_sent_mutex, trajectory_set_mutex,
                    current_trajectory_mutex, traj_callback_mutex, last_goal_sent_mutex, following_index_mutex,
                    ranges_mutex, previous_angle_mutex, n_obs_counter_mutex, Yaw_mutex, goal_with_yaw_mutex, last_goal_with_yaw_mutex,
                    final_goal_mutex;

    geometry_msgs::PoseStamped goal_with_yaw, last_goal_with_yaw;
    traj_points last_sent;
    int status_prev, goal_reached_counter;

    bool globalGoal;


    //subscribers
protected:
    //Subscriber
    ros::Subscriber droneCmdVelSub;
    ros::Subscriber droneRotAngSub;
    ros::Subscriber droneOdomSub;
    ros::Subscriber droneTrajSub;
    ros::Subscriber droneScanSub;
    ros::Subscriber droneResetSub;
    ros::Subscriber droneGoalSub;
    ros::Subscriber droneGlobalSub;
    ros::Publisher droneYawPub;
    ros::Publisher droneVelPub;
    ros::Publisher dronePitchRollPub;
    ros::Publisher droneAltitudePub;
    ros::Publisher droneOdomPub;
    ros::Publisher dronePositionPub;
    ros::Publisher droneObsPub;
    ros::Publisher droneGoalPub;
    ros::Publisher droneSubGoalPub;

//    void droneVelCallback(geometry_msgs::Twist::ConstPtr msg);
    void droneRotCallback(geometry_msgs::Vector3Stamped::ConstPtr msg);
    void droneOdomCallback(nav_msgs::Odometry::ConstPtr msg);
    void droneTrajCallback(nav_msgs::Path::ConstPtr msg);
    void droneScanCallback(sensor_msgs::LaserScan::ConstPtr msg);
//    void droneResetCallback(std_msgs::Bool::ConstPtr msg);
    void droneGoalCallback(geometry_msgs::PoseStamped::ConstPtr msg);
    void droneGlobalCallback(geometry_msgs::PoseStamped::ConstPtr msg);
    void onObstacleSolver();
    void stateCleaner();
    void subgoalsOnlySend(double x, double y, bool was_me_param);
    void sendGoal(double x, double y, bool was_me_param);
    void subgoalsEraseFrontAndSend();
    void subgoalsAddElement(std::vector<traj_points> subsubgoals);
    void subgoalsClear();
    void subgoalsEraseBack();
    void sendMainGoalAgain();
    bool lower_than (traj_points i, traj_points j);
    template <typename T>
    std::vector<T> SimpleMovingAverage(std::vector<T> points, int window_size);
    int SharpCornersTrajectoryChecking(int window_size, int dist_bet_inflection, int dist_bet_max, int N_lines, double angle_max);
    template <typename T>
    std::vector<T> diff(std::vector<T> x);
    template <typename T>
    std::vector<T> abs_diff(std::vector<T> x);
    template <typename T>
    std::vector<int> find(std::vector<T> x, int condition, T value);
    line getLineParamFromPoints(double x1, double x2, double y1, double y2);
    std::vector<double> getLinePointsFromParams(line *Line, std::vector<double> x);
    template <typename T>
    int min(std::vector<T> x);
    void publishYaw(droneMsgsROS::droneYawRefCommand yaw);


public:
    DroneNavigationStackROSModule();
    ~DroneNavigationStackROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};


