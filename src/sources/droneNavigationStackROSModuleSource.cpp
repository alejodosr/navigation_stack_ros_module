//////////////////////////////////////////////////////
///  droneNavigationStackROSModuleSource.cpp
///
///  Created on: May 14, 2016
///      Author: alejodosr
///
///  Last modification on: Aug 11, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////


#include "droneNavigationStackROSModuleHeader.h"

using namespace std;

DroneNavigationStackROSModule::DroneNavigationStackROSModule() : DroneModule(droneModule::active)
{

    return;
}


DroneNavigationStackROSModule::~DroneNavigationStackROSModule()
{
    close();
    return;
}

bool DroneNavigationStackROSModule::init()
{
    DroneModule::init();

    return true;
}

template <typename T>
std::vector<T> DroneNavigationStackROSModule::SimpleMovingAverage(std::vector<T> traj, int window_size){

    // Variables definition
    vector<T> moving_window;
    vector<T> out_traj;

    if (window_size < traj.size()){
        // Initialize the moving window
        for (int i=0; i < window_size; i++){
            moving_window.push_back((T) 0);
        }

        for (int i=0; i < traj.size(); i++){
            // Shift moving window
            for(int j=1; j < moving_window.size(); j++){
                moving_window[j-1] = moving_window[j];
            }
            moving_window[moving_window.size() - 1] = traj[i];

            // Calculate average
            double avg = 0;
            for(int j=0; j < moving_window.size(); j++){
                avg += moving_window[j];
            }
            avg /= moving_window.size();
            out_traj.push_back(avg);
        }

        // Return the sma vector
        return out_traj;
    }
    else{
        // Return the sma vector
        return traj;
    }
}

template <typename T>
std::vector<T> DroneNavigationStackROSModule::diff(std::vector<T> x){
    vector<T> x_diff;
    for (int i=1; i < x.size(); i++){
        x_diff.push_back(x[i] - x[i-1]);
    }
    return x_diff;
}

template <typename T>
std::vector<T> DroneNavigationStackROSModule::abs_diff(std::vector<T> x){
    vector<T> x_diff;
    for (int i=1; i < x.size(); i++){
        x_diff.push_back(abs(x[i] - x[i-1]));
    }
    return x_diff;
}

template <typename T>
int DroneNavigationStackROSModule::min(std::vector<T> x){
    T min = (T) 1E9;
    int index_min;
    for(int i = 0; i < x.size(); i++){
        if (x[i] < min){
            min = x[i];
            index_min = i;
        }
    }

    return index_min;
}

template <typename T>
std::vector<int> DroneNavigationStackROSModule::find(std::vector<T> x, int condition, T value){
    vector<int> indices;
    for(int i=0; i < x.size(); i++){
        switch ( condition )
        {
        case LOWER:
            if (x[i] < value){
                indices.push_back(i);
            }
            break;
        case LOWER_EQUAL:
            if (x[i] <= value){
                indices.push_back(i);
            }
            break;
        case EQUAL:
            if (x[i] == value){
                indices.push_back(i);
            }
            break;
        case HIGHER_EQUAL:
            if (x[i] >= value){
                indices.push_back(i);
            }
            break;
        case HIGHER:
            if (x[i] > value){
                indices.push_back(i);
            }
            break;
        default:
            indices.clear();
        }
    }

    return indices;
}

line DroneNavigationStackROSModule::getLineParamFromPoints(double x1, double x2, double y1, double y2){
    line Line;
    Line.m = (y2 - y1) / (x2 - x1);
    Line.o = y1 - Line.m * x1;

    return Line;
}

vector<double> DroneNavigationStackROSModule::getLinePointsFromParams(line *Line, vector<double> x){
    vector<double> line_out;
    for (int i=0; i < x.size(); i++){
        line_out.push_back(Line->m * x[i] + Line->o);
    }
    return line_out;
}

int DroneNavigationStackROSModule::SharpCornersTrajectoryChecking(int window_size = WINDOW_SIZE,
                                                                  int dist_bet_inflection = DIST_BET_INFLECTION, int dist_bet_max = DIST_BET_MAX,
                                                                  int N_lines = N_LINES, double angle_max = ANGLE_MAX){
    /// Variables declaration
    vector<traj_points> subsubgoals;

    /// Fill vectors with the trajectory
    vector<double> x, y;

#ifdef DEBUG_MODE
    traj_num++;
    // Filling a file with the trajectory
    //            char buffer [50];
    //            sprintf (buffer, "/home/cucumber/traj%d.csv", traj_num);
    //            File.open (buffer, ios::out | ios::ate | ios::app) ;
    //            File << "x,y" << endl;
    //            File.close();
    //            File.open (buffer, ios::out | ios::ate | ios::app) ;
#endif

    //Lock trajectory
    current_trajectory_mutex.lock();
    for (int i=0; i<current_trajectory.poses.size(); i++){
        x.push_back(current_trajectory.poses[i].pose.position.x);
        y.push_back(current_trajectory.poses[i].pose.position.y);
#ifdef DEBUG_MODE
        //                File << current_trajectory.poses[i].pose.position.x << "," << current_trajectory.poses[i].pose.position.y << endl;
#endif
    }
    //Unlock trajectory
    current_trajectory_mutex.unlock();

#ifdef DEBUG_MODE
    //            File.close();
#endif

    /// Smooth a little the trajectory
    vector<double> x_soft, y_soft;
    x_soft = SimpleMovingAverage(x, 10);
    y_soft = SimpleMovingAverage(y, 10);

    /// Extract the middle point of the sharp curves
    // Get it smoothed
    vector<double> avg_x, avg_y;
    avg_x = SimpleMovingAverage(abs_diff(diff(x_soft)), window_size);
    avg_y = SimpleMovingAverage(abs_diff(diff(y_soft)), window_size);

    // Find the maximums
    vector<int> zero_slope_i_y, zero_slope_i_x, zero_slope_i_y_h, zero_slope_i_x_h, zero_slope_i_y_final, zero_slope_i_x_final;
    zero_slope_i_y = find(diff(avg_y), HIGHER, 0.d);
    zero_slope_i_y_h = find(diff(zero_slope_i_y), HIGHER, dist_bet_max);
    for(int i = 0; i < zero_slope_i_y_h.size(); i++){
        zero_slope_i_y_final.push_back(zero_slope_i_y[zero_slope_i_y_h[i] - ceil((double) window_size / (double) 2 )]);
    }

    zero_slope_i_x = find(diff(avg_x), HIGHER, 0.d);
    zero_slope_i_x_h = find(diff(zero_slope_i_x), HIGHER, dist_bet_max);
    for(int i = 0; i < zero_slope_i_x_h.size(); i++){
        zero_slope_i_x_final.push_back(zero_slope_i_x[zero_slope_i_x_h[i] - ceil((double) window_size / (double) 2 )]);
    }

    // Final vector
    vector<int> zero_slope_i;
    for(int i = 0; i < zero_slope_i_x_final.size(); i++){
        if (zero_slope_i_x_final[i] >= 0)   zero_slope_i.push_back(zero_slope_i_x_final[i]);
    }
    for(int i = 0; i < zero_slope_i_y_final.size(); i++){
        if (zero_slope_i_y_final[i] >= 0)   zero_slope_i.push_back(zero_slope_i_y_final[i]);
    }

#ifdef DEBUG_MODE
    /// Debugging purposes
    // Filling a file with the trajectory
    //    sprintf (buffer, "/home/cucumber/anchor_traj%d.csv", traj_num + 1);
    //    File.open (buffer, ios::out | ios::ate | ios::app) ;
    //    File << "x_anchor,y_anchor" << endl;
    //    File.close();
    //    File.open (buffer, ios::out | ios::ate | ios::app) ;
#endif

    //    for (int i = 0; i < zero_slope_i.size(); i++){
    //        File << x[zero_slope_i[i]] << "," << y[zero_slope_i[i]] << endl;
    //    }
    //    File.close();


    // Filling a file with the trajectory
    //    char buffer [50];
    //    sprintf (buffer, "/home/cucumber/y_zero_traj%d.csv", traj_num + 1);
    //    File.open (buffer, ios::out | ios::ate | ios::app) ;
    //    File << "x,y_zero" << endl;
    //    File.close();

    ///

    // Vector declaration
    vector<int>     inter_avg_vector;

    for(int j=0; j < zero_slope_i.size(); j++){
        if(zero_slope_i[j] + 1 < x.size()){
            /// Finding the slope to the line tangent to the sharp-curve
            line Line_zero;
            Line_zero = getLineParamFromPoints(x[zero_slope_i[j]],
                    x[zero_slope_i[j] + 1],
                    y[zero_slope_i[j]],
                    y[zero_slope_i[j] + 1]);
            vector<double> y_zero = getLinePointsFromParams(&Line_zero, x);

            /// Debugging purposes
            //        File.open (buffer, ios::out | ios::ate | ios::app) ;
            //        for (int i = 0; i < y_zero.size(); i++){
            //            File << x[i] << "," << y_zero[i] << endl;
            //        }
            //        File.close();

            ///

            /// Loop to find lines and angles
            vector<int> intersec_avg_r, intersec_avg_l;
            intersec_avg_l.clear();
            intersec_avg_r.clear();

            for(int i = 1; i <= N_lines; i++){
                /// Find the line
                if(zero_slope_i[j] - i * dist_bet_inflection >= 0){
                    line Line2;
                    Line2 = getLineParamFromPoints(x[zero_slope_i[j] - i * dist_bet_inflection],
                            x[zero_slope_i[j] - i * dist_bet_inflection + 1],
                            y[zero_slope_i[j] - i * dist_bet_inflection],
                            y[zero_slope_i[j] - i * dist_bet_inflection + 1]);
                    vector<double> y_tangent = getLinePointsFromParams(&Line2, x);

                    /// Find the intersection point to the y_zero
                    vector<double> substraction;
                    for(int i = 0; i < x.size(); i++){
                        substraction.push_back(abs(y_tangent[i] - y_zero[i]));
                    }

                    int intersec_i = min(substraction);

                    if(intersec_i < x.size() - 1){
                        /// Find the angle
                        double x1 = x[intersec_i + 1] - x[intersec_i];
                        double x2 = x[intersec_i + 1] - x[intersec_i];
                        double y1 = y_zero[intersec_i + 1] - y_zero[intersec_i];
                        double y2 = y_tangent[intersec_i + 1] - y_tangent[intersec_i];
                        double dot = x1*x2 + y1*y2;
                        double det = x1*y2 - y1*x2;
                        double angle = 180.d - abs(atan2(det,dot) * 180.d / (double) M_PI);

                        if (angle < angle_max){
                            //                        if (zero_slope_i[j] - SEPARATION_FROM_ANCHOR >= 0){
                            //                            intersec_avg_l.push_back(intersec_i);
                            //                            intersec_avg_l.push_back(zero_slope_i[j] - SEPARATION_FROM_ANCHOR);
                            intersec_avg_l.push_back(1);
                            //                        }
                            //                        if (zero_slope_i[j] + SEPARATION_FROM_ANCHOR <= x.size() - 1){
                            //                            intersec_avg_r.push_back(intersec_i);
                            //                            intersec_avg_r.push_back(zero_slope_i[j] + SEPARATION_FROM_ANCHOR);
                            intersec_avg_r.push_back(1);
                            //                        }
                        }
                    }
                }

                /// Find the line
                if(zero_slope_i[j] + i * dist_bet_inflection < x.size() - 1){
                    line Line2;
                    Line2 = getLineParamFromPoints(x[zero_slope_i[j] + i * dist_bet_inflection],
                            x[zero_slope_i[j] + i * dist_bet_inflection + 1],
                            y[zero_slope_i[j] + i * dist_bet_inflection],
                            y[zero_slope_i[j] + i * dist_bet_inflection + 1]);
                    vector<double> y_tangent = getLinePointsFromParams(&Line2, x);

                    /// Find the intersection point to the y_zero
                    vector<double> substraction;
                    for(int i = 0; i < x.size(); i++){
                        substraction.push_back(abs(y_tangent[i] - y_zero[i]));
                    }
                    int intersec_i = min(substraction);

                    if(intersec_i < x.size() - 1){
                        /// Find the angle
                        double x1 = x[intersec_i + 1] - x[intersec_i];
                        double x2 = x[intersec_i + 1] - x[intersec_i];
                        double y1 = y_zero[intersec_i + 1] - y_zero[intersec_i];
                        double y2 = y_tangent[intersec_i + 1] - y_tangent[intersec_i];
                        double dot = x1*x2 + y1*y2;
                        double det = x1*y2 - y1*x2;
                        double angle = 180.d - abs(atan2(det,dot) * 180.d / (double) M_PI);

                        if (angle < angle_max){
                            //                        if (zero_slope_i[j] + SEPARATION_FROM_ANCHOR <= x.size() - 1){
                            //                            intersec_avg_r.push_back(intersec_i);
                            //                            intersec_avg_r.push_back(zero_slope_i[j] + SEPARATION_FROM_ANCHOR);
                            intersec_avg_r.push_back(1);
                            //                        }
                            //                        if (zero_slope_i[j] - SEPARATION_FROM_ANCHOR >= 0){
                            //                            intersec_avg_l.push_back(intersec_i);
                            //                            intersec_avg_l.push_back(zero_slope_i[j] - SEPARATION_FROM_ANCHOR);
                            intersec_avg_l.push_back(1);
                            //                        }
                        }
                    }
                }
            }

            // Reset inter_avg_vector
            inter_avg_vector.clear();

            /// Average the position of the goals to avoid duplicity
            if (intersec_avg_l.size() > 0){
                int inter_avg = 0;
                for(int i = 0; i < intersec_avg_l.size(); i++){
                    inter_avg += intersec_avg_l[i];
                }
                inter_avg /= intersec_avg_l.size();
                inter_avg_vector.push_back(inter_avg);


                //            traj_points tp;
                //            tp.x = x[inter_avg];
                //            tp.y = y_zero[inter_avg];

                //            /// Fill a vector with the distance to get the closest point to the trajectory
                //            vector<double> distances;
                //            for (int i = 0; i < x.size(); i++){
                //                distances.push_back(sqrt(pow((x[inter_avg] - x[i]),2) +  pow((y_zero[inter_avg] - y[i]),2)));
                //            }
                //            int min_i = min(distances);
                //            tp.x_closest = x[min_i];
                //            tp.y_closest = y[min_i];

                //            subsubgoals.push_back(tp);
            }
            if (intersec_avg_r.size() > 0){
                int inter_avg = 0;
                for(int i = 0; i < intersec_avg_r.size(); i++){
                    inter_avg += intersec_avg_r[i];
                }
                inter_avg /= intersec_avg_r.size();
                inter_avg_vector.push_back(inter_avg);


                //            traj_points tp;
                //            tp.x = x[inter_avg];
                //            tp.y = y_zero[inter_avg];

                //            /// Fill a vector with the distance to get the closest point to the trajectory
                //            vector<double> distances;
                //            for (int i = 0; i < x.size(); i++){
                //                distances.push_back(sqrt(pow((x[inter_avg] - x[i]),2) +  pow((y_zero[inter_avg] - y[i]),2)));
                //            }
                //            int min_i = min(distances);
                //            tp.x_closest = x[min_i];
                //            tp.y_closest = y[min_i];

                //            subsubgoals.push_back(tp);
            }

            if (inter_avg_vector.size() > 0){
                /// Sorting the inter_avg_vector
                //            for (int i=0; i<inter_avg_vector.size();i++){
                //            std::sort (inter_avg_vector.begin(), inter_avg_vector.end(), bind(&DroneNavigationStackROSModule::lower_than, this, _1, _2));
                //            }
                //            std::vector<int> ind(inter_avg_vector.size());
                //            std::iota(ind.begin(), ind.end(), 0);
                //            auto comparator = [&inter_avg_vector](int a, int b){ return inter_avg_vector[a] < inter_avg_vector[b]; };
                //            std::sort(inter_avg_vector.begin(), inter_avg_vector.end());

                /// Adding the subgoals
                //            for (std::vector<int>::const_iterator it=inter_avg_vector.begin(); it!=inter_avg_vector.end(); ++it){
                //            for(int i = 0; i< inter_avg_vector.size(); i++){
                /// Right side of the anchor
                double x_plotted = x[zero_slope_i[j]] + (double) D_FROM_ANCHOR / sqrt(pow(Line_zero.m, 2) + 1);
                double y_plotted = Line_zero.m * x_plotted + Line_zero.o;

                traj_points tp;
                tp.x = x_plotted;
                tp.y = y_plotted;

                /// Fill a vector with the distance to get the closest point to the trajectory
                vector<double> distances;
                for (int k = 0; k < x.size(); k++){
                    distances.push_back(sqrt(pow((x_plotted - x[k]),2) +  pow((y_plotted  - y[k]),2)));
                }
                int min_i = min(distances);
                tp.x_closest = x[min_i];
                tp.y_closest = y[min_i];

                tp.index = min_i;

                /// Test if the last one is close, then do not add it
                traj_points prev;
                if(subsubgoals.size() > 0){
                    prev = subsubgoals.back();
                }
                else{
                    prev.x = current_pose.pose.position.x;
                    prev.y = current_pose.pose.position.y;
                }
                if(abs(prev.x - tp.x) > SUBGOALS_TOL){
                    if(abs(prev.y - tp.y) > SUBGOALS_TOL){
                        /// Add subgoal
                        subsubgoals.push_back(tp);
                    }
                }
                //            else    /// Add subgoal
                //                subsubgoals.push_back(tp);



                /// Left side of the anchor
                x_plotted = x[zero_slope_i[j]] - (double) D_FROM_ANCHOR / sqrt(pow(Line_zero.m, 2) + 1);
                y_plotted = Line_zero.m * x_plotted + Line_zero.o;

                tp.x = x_plotted;
                tp.y = y_plotted;

                /// Fill a vector with the distance to get the closest point to the trajectory
                distances.clear();
                for (int k = 0; k < x.size(); k++){
                    distances.push_back(sqrt(pow((x_plotted - x[k]),2) +  pow((y_plotted  - y[k]),2)));
                }
                min_i = min(distances);
                tp.x_closest = x[min_i];
                tp.y_closest = y[min_i];

                tp.index = min_i;

                /// Test if the last one is close, then do not add it
                if(subsubgoals.size() > 0){
                    prev = subsubgoals.back();
                }
                else{
                    prev.x = current_pose.pose.position.x;
                    prev.y = current_pose.pose.position.y;
                }
                if(abs(prev.x - tp.x) > SUBGOALS_TOL){
                    if(abs(prev.y - tp.y) > SUBGOALS_TOL){
                        /// Add subgoal
                        subsubgoals.push_back(tp);
                    }
                }
                //            else    /// Add subgoal
                //                subsubgoals.push_back(tp);
                //            }
            }
        }
    }

    if(subsubgoals.size() > 0){
        /// Sorting vector
        std::sort (subsubgoals.begin(), subsubgoals.end(), bind(&DroneNavigationStackROSModule::lower_than, this, _1, _2));
    }

    /// Adding subgoals to the class member
    if (subsubgoals.size() > 0) {
        // Include the last goal
        traj_points tp;
        tp.x = x[x.size() - 1];
        tp.y = y[x.size() - 1];
        tp.x_closest = x[x.size() - 1];
        tp.y_closest = y[x.size() - 1];
        subsubgoals.push_back(tp);

        // Add subgoals
        subgoalsAddElement(subsubgoals);

        return 1;
    }
    else    return 0;
}

bool DroneNavigationStackROSModule::lower_than (traj_points i, traj_points j) { return (i.index < j.index); }

void DroneNavigationStackROSModule::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }

    /// Position controller strategy
    ///
    // Lock mutex
    trajectory_set_mutex.lock();
    // Update state variable
    trajectory_set = false;
    // Unlock mutex
    trajectory_set_mutex.unlock();
    previous_pose.pose.position.x = 0;
    previous_pose.pose.position.y = 0;
    previous_pose.pose.position.z = 0;
    previous_angle = 0;
    dont_move = false;
    dont_yaw = false;
    timeout = ros::Time::now();
    status = STATUS_CLOSE;
    x_tolerance = X_TOLERANCE_CLOSE;
    y_tolerance = Y_TOLERANCE_CLOSE;

    //// TOPICS
    //Subscribers
    //    droneCmdVelSub = n.subscribe("/cmd_vel", 1, &DroneNavigationStackROSModule::droneVelCallback, this);
    droneOdomSub = n.subscribe("odometry/filtered", 1, &DroneNavigationStackROSModule::droneOdomCallback, this);
    droneRotAngSub = n.subscribe("rotation_angles", 1, &DroneNavigationStackROSModule::droneRotCallback, this);
    droneTrajSub = n.subscribe("move_base/NavfnROS/plan", 1, &DroneNavigationStackROSModule::droneTrajCallback, this);
    droneScanSub = n.subscribe("scan", 1, &DroneNavigationStackROSModule::droneScanCallback, this);
    //    droneResetSub   = n.subscribe("reset", 1, &DroneNavigationStackROSModule::droneResetCallback, this);
    droneGoalSub   = n.subscribe("move_base_simple/goal", 1, &DroneNavigationStackROSModule::droneGoalCallback, this);
    droneGlobalSub   = n.subscribe("globalGoal", 1, &DroneNavigationStackROSModule::droneGlobalCallback, this);


    //Publishers
    droneYawPub=n.advertise<droneMsgsROS::droneYawRefCommand>("droneControllerYawRefCommand", 1, true);
    droneVelPub=n.advertise<droneMsgsROS::droneSpeeds>("droneSpeedsRefs", 1, true);
    //    dronePitchRollPub=n.advertise<droneMsgsROS::dronePitchRollCmd>("command/pitch_roll", 1, true);
    //    droneYawPub=n.advertise<droneMsgsROS::droneDYawCmd>("command/dYawStack", 1, true);
    //    droneAltitudePub=n.advertise<droneMsgsROS::droneDAltitudeCmd>("command/dAltitude", 1, true);
    droneOdomPub=n.advertise<nav_msgs::Odometry>("odom", 1, true);
    dronePositionPub   = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1);
    droneObsPub   = n.advertise<std_msgs::Bool>("obstacle_found", 1);
    droneGoalPub   = n.advertise<std_msgs::Bool>("goal_reached", 1);
    droneSubGoalPub = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal", 1);


    //Flag of module opened
    droneModuleOpened=true;

    //autostart module!
    //moduleStarted=true;

    traj_num = 3;
    n_obs_counter_mutex.lock();
    n_obs_counter = 0;
    n_obs_counter_mutex.unlock();

    // Initializing state variables
    subtrajectory_mutex.lock();
    subtrajectory = false;
    subtrajectory_mutex.unlock();

    goal_received_mutex.lock();
    goal_received = false;
    goal_received_mutex.unlock();

    was_me_mutex.lock();
    was_me = false;
    was_me_mutex.unlock();

    subgoals_mutex.lock();
    subgoals.clear();
    subgoals_mutex.unlock();

    last_sub_goal_sent_mutex.unlock();
    trajectory_set_mutex.unlock();
    current_trajectory_mutex.unlock();
    traj_callback_mutex.unlock();

    last_goal_sent_mutex.lock();
    last_goal_sent.x = 1E9;
    last_goal_sent.y = 1E9;
    last_goal_sent_mutex.unlock();

    following_index_mutex.unlock();
    ranges_mutex.unlock();
    previous_angle_mutex.unlock();
    n_obs_counter_mutex.unlock();
    Yaw_mutex.unlock();
    goal_with_yaw_mutex.unlock();
    last_goal_with_yaw_mutex.unlock();

    status_prev = STATUS_CLOSE;

    globalGoal = false;

    final_goal = false;
    final_goal_mutex.unlock();

		goal_reached_counter = 0;

    //End
    return;
}

//void DroneNavigationStackROSModule::droneResetCallback(std_msgs::Bool::ConstPtr msg){
//#ifdef DEBUG_MODE
//    cout << "TRAJECTORY PLANNER HAS BEEN RESET" << endl;
//#endif
//    // No matter the value of the topic, reset the module
//    new_subsubgoals = false;
//    subgoals.clear();
//    trajectory_set = false;
//}

void DroneNavigationStackROSModule::sendGoal(double x, double y, bool was_me_param){
    // Fill the message
    geometry_msgs::PoseStamped goal;
    goal.header.stamp = ros::Time::now();
    goal.header.frame_id = "map";
    goal.pose.position.x = x;
    goal.pose.position.y = y;
    goal.pose.position.z = last_goal_sent.z;

    goal.pose.orientation.x = 0;
    goal.pose.orientation.y = 0;
    goal.pose.orientation.z = 0;
    goal.pose.orientation.w = 1;

    // Lock the mutex
    was_me_mutex.lock();
    // Update the state
    was_me = was_me_param;
    // Unlock the mutex
    was_me_mutex.unlock();
    // Publish the subgoal
    droneSubGoalPub.publish(goal);
}

void DroneNavigationStackROSModule::droneGoalCallback(geometry_msgs::PoseStamped::ConstPtr msg){
    // Updating state variable when a goal is received
    /*goal_received_mutex.lock();
    last_goal_z = msg->pose.position.z;
    goal_received = true;
    goal_received_mutex.unlock();

    goal_with_yaw_mutex.lock();
    goal_with_yaw = *msg;
    goal_with_yaw_mutex.unlock();*/
}

void DroneNavigationStackROSModule::droneGlobalCallback(geometry_msgs::PoseStamped::ConstPtr msg){
    if(moduleStarted){
        // Updating state variable when a goal is received
        goal_received_mutex.lock();
        last_goal_z = msg->pose.position.z;
        goal_received = true;
        goal_received_mutex.unlock();

        goal_with_yaw_mutex.lock();
        goal_with_yaw = *msg;
        goal_with_yaw_mutex.unlock();


        // Lock the mutex
        current_trajectory_mutex.lock();
        last_goal_sent_mutex.lock();
        // Store last goal
        last_goal_sent.x = goal_with_yaw.pose.position.x;
        last_goal_sent.y = goal_with_yaw.pose.position.y;
        last_goal_sent.z = last_goal_z;
        // Unlock trajectory
        last_goal_sent_mutex.unlock();
        current_trajectory_mutex.unlock();
//#ifdef DEBUG_MODE
        last_goal_sent_mutex.lock();
        cout << "LAST GOAL CHANGED!  " << last_goal_sent.x << "   " << last_goal_sent.y << "   " << last_goal_sent.z << endl;
        last_goal_sent_mutex.unlock();
//#endif
        // Update goal with yaw
        last_goal_with_yaw_mutex.lock();
        goal_with_yaw_mutex.lock();
        // Update variable
        last_goal_with_yaw = goal_with_yaw;
        // Unlock mutex
        last_goal_with_yaw_mutex.unlock();
        goal_with_yaw_mutex.unlock();

        // Send last goal to Navigation Stack
        sendGoal(last_goal_sent.x, last_goal_sent.y, false);

        // Only Global Mission planner can assign goals
        globalGoal = true;
    }
}

void DroneNavigationStackROSModule::subgoalsClear(){
    // Subgoals is cleared safely
    subgoals_mutex.lock();
    subgoals.clear();
    subgoals_mutex.unlock();

    // Updating subtrajectory
    subtrajectory_mutex.lock();
    subtrajectory = false;
    subtrajectory_mutex.unlock();

    // Lock the mutex
    n_obs_counter_mutex.lock();
    // Reset obstacle counter
    n_obs_counter = 0;
    // Unlock the mutex
    n_obs_counter_mutex.unlock();
}

void DroneNavigationStackROSModule::subgoalsAddElement(vector<traj_points> subsubgoals){
    /// Update subgoals
    // Lock, update and unlock mutex
    subgoals_mutex.lock();
    subgoals.push_back(subsubgoals);
    subgoals_mutex.unlock();

    /// Update state variables
    subtrajectory_mutex.lock();
    subtrajectory = true;
    subtrajectory_mutex.unlock();
}

void DroneNavigationStackROSModule::subgoalsEraseFrontAndSend(){
    /// Lock subgoals mutex
    subgoals_mutex.lock();

    /// Get it from subgoals member
    vector<traj_points> subsubgoals;
    subsubgoals = subgoals.back();
    traj_points tp = subsubgoals.front();

    /// Store the last subgoal sent
    last_sub_goal_sent_mutex.lock();
    last_sub_goal_sent = tp;
    last_sub_goal_sent_mutex.unlock();

    /// Debugging purposes
    // Filling a file with the trajectory
    //    char buffer [50];
    //    sprintf (buffer, "/home/cucumber/sub_traj%d.csv", traj_num + 1);
    //    File.open (buffer, ios::out | ios::ate | ios::app) ;
    //    File << "x_subgoal,y_subgoal" << endl;
    //    File.close();
    //    File.open (buffer, ios::out | ios::ate | ios::app) ;


    for (int i = 0; i < subsubgoals.size(); i++){
        cout << "subsubgoals:  X: " << subsubgoals[i].x << "\tY: " << subsubgoals[i].y << "\tIndex: " << subsubgoals[i].index << endl;
        //        File << subsubgoals[i].x << "," << subsubgoals[i].y << endl;
    }
    //    File.close();
    ///

    /// Erase the first element
    subsubgoals.erase(subsubgoals.begin());

    /// Update the subgoals vector
    subgoals.erase(subgoals.end());
    if (subsubgoals.size() > 0) subgoals.push_back(subsubgoals);

    /// Updating state variables
    if (subgoals.size() == 0){
        subtrajectory_mutex.lock();
        subtrajectory = false;
        subtrajectory_mutex.unlock();
    }

    /// Store last subgoal sent
    last_sent.x = tp.x;
    last_sent.y = tp.y;
    last_sent.x_closest = tp.x_closest;
    last_sent.y_closest = tp.y_closest;

    /// Send a new goal
    sendGoal(tp.x, tp.y, true);

    // Lock the mutex
    n_obs_counter_mutex.lock();
    /// New subgoal sent, obstacle counter should be reset
    n_obs_counter = 0;
    // Unlock the mutex
    n_obs_counter_mutex.unlock();

#ifdef DEBUG_MODE
    cout << "New subgoal sent!  " << tp.x << "   " << tp.y <<  endl;
    cout << "Subsubgoals size: " << subsubgoals.size() << endl;
    cout << "Subgoals size: " << subgoals.size() << endl;
#endif

    /// Unlock subgoals mutex
    subgoals_mutex.unlock();
}

void DroneNavigationStackROSModule::subgoalsOnlySend(double x, double y, bool was_me_param){
    /// Send a new goal
    sendGoal(x, y, was_me_param);

#ifdef DEBUG_MODE
    cout << "New subgoal MOVED!  " << x << "   " << y <<  endl;
#endif
}

void DroneNavigationStackROSModule::subgoalsEraseBack(){
    /// Lock subgoals mutex
    subgoals_mutex.lock();

    /// Update the subgoals vector
    subgoals.erase(subgoals.end());

    /// Unlock subgoals mutex
    subgoals_mutex.unlock();
}

void DroneNavigationStackROSModule::sendMainGoalAgain(){
#ifdef DEBUG_MODE
    last_goal_sent_mutex.lock();
    cout << "MAX subgoals movements! Sending: " << last_goal_sent.x << "   " << last_goal_sent.y <<  endl;
    last_goal_sent_mutex.unlock();
#endif

    /// Clear the state
    //    subgoalsClear();

    /// Send a new goal
    last_goal_sent_mutex.lock();
    subgoalsOnlySend(last_goal_sent.x, last_goal_sent.y, false);
    last_goal_sent_mutex.unlock();
}

void DroneNavigationStackROSModule::stateCleaner(){
#ifdef DEBUG_MODE
    cout << "------------- STATE CLEANER ---------------" << endl;
    // Lock the mutex
    was_me_mutex.lock();
    cout << "was_me: " << was_me << endl;
    // Unlock the mutex
    was_me_mutex.unlock();
    // Updating state variable when a goal is received
    goal_received_mutex.lock();
    cout << "goal_received: " << goal_received << endl;
    // Unlock the variable
    goal_received_mutex.unlock();
    cout << "-------------------------------------------" << endl;
#endif
    // Lock the mutex
    was_me_mutex.lock();
    if (!was_me){
        // Unlock the mutex
        was_me_mutex.unlock();

        // Updating state variable when a goal is received
        goal_received_mutex.lock();
        if(goal_received && globalGoal){

            //            // Lock the mutex
            //            current_trajectory_mutex.lock();
            //            last_goal_sent_mutex.lock();
            //            // Store last goal
            //            last_goal_sent.x = current_trajectory.poses[current_trajectory.poses.size() - 1].pose.position.x;
            //            last_goal_sent.y = current_trajectory.poses[current_trajectory.poses.size() - 1].pose.position.y;
            //            last_goal_sent.z = last_goal_z;
            //            // Unlock trajectory
            //            last_goal_sent_mutex.unlock();
            //            current_trajectory_mutex.unlock();
            //#ifdef DEBUG_MODE
            //            last_goal_sent_mutex.lock();
            //            cout << "LAST GOAL CHANGED!  " << last_goal_sent.x << "   " << last_goal_sent.y << "   " << last_goal_sent.z << endl;
            //            last_goal_sent_mutex.unlock();
            //#endif
            //            // Update goal with yaw
            //            last_goal_with_yaw_mutex.lock();
            //            goal_with_yaw_mutex.lock();
            //            // Update variable
            //            last_goal_with_yaw = goal_with_yaw;
            //            // Unlock mutex
            //            last_goal_with_yaw_mutex.unlock();
            //            goal_with_yaw_mutex.unlock();

        }
        // Unlock the variable
        goal_received_mutex.unlock();

        //        // Check state variable
        //        subtrajectory_mutex.lock();
        //        if (subtrajectory){
        //            // Unlock the mutex
        //            subtrajectory_mutex.unlock();
        //            //Lock trajectory
        //            current_trajectory_mutex.lock();
        //#ifdef DEBUG_MODE
        //    cout << "Last sent: " << last_sent.x << "   " << last_sent.y <<  endl;
        //    cout << "Current trajectory: " << current_trajectory.poses[current_trajectory.poses.size() - 1].pose.position.x << "   " << current_trajectory.poses[current_trajectory.poses.size() - 1].pose.position.y <<  endl;
        //#endif
        //            // Check if the goal is the same as subgoal sent
        //            if(!(current_trajectory.poses[current_trajectory.poses.size() - 1].pose.position.x - last_sent.x < 0.1) ||
        //                    !(current_trajectory.poses[current_trajectory.poses.size() - 1].pose.position.y - last_sent.y < 0.1)){
        //                //Unlock trajectory
        //                current_trajectory_mutex.unlock();
        //                // Clear the state
        //                subgoalsClear();
        //            }
        //            else{
        //                //Unlock trajectory
        //                current_trajectory_mutex.unlock();
        //#ifdef DEBUG_MODE
        //                cout << "Es la misma trayectoria, lo siento :( " << last_sent.x << "   " << last_sent.y <<  endl;
        //#endif
        //                subgoalsEraseBack();
        //            }
        //        }
        //        else{
        //            // Unlock the mutex
        //            subtrajectory_mutex.unlock();
        // Clear the state
        subgoalsClear();
        //        }
    }
    else{   // was_me has to be reset
        was_me = false;
        // Unlock the mutex
        was_me_mutex.unlock();
        // Updating state variable when a goal is received
        goal_received_mutex.lock();
        if(!goal_received){
            // Clear the state
            subgoalsClear();
        }
        // Unlock the variable
        goal_received_mutex.unlock();
    }

    //Lock state variable
    goal_received_mutex.lock();
    //Update state variable
    goal_received = false;
    // Unlock state variable
    goal_received_mutex.unlock();
}

void DroneNavigationStackROSModule::publishYaw(droneMsgsROS::droneYawRefCommand yaw){
    droneYawPub.publish(yaw);
}

void DroneNavigationStackROSModule::onObstacleSolver(){
    // Lock the mutex
    subtrajectory_mutex.lock();
    if(subtrajectory){
        // Unlock the mutex
        subtrajectory_mutex.unlock();
        // Lock mutex
        last_sub_goal_sent_mutex.lock();
        // Calculate point
        double x = last_sub_goal_sent.x/2.d + last_sub_goal_sent.x_closest/2.d;
        double y = last_sub_goal_sent.y/2.d + last_sub_goal_sent.y_closest/2.d;
        // Update last subgoal
        last_sub_goal_sent.x = x;
        last_sub_goal_sent.y = y;
        //Unlock mutex
        last_sub_goal_sent_mutex.unlock();
        // Lock the mutex
        n_obs_counter_mutex.lock();
        // Counter number of movements
        n_obs_counter++;
        // Moved subgoal has a limit
        if(n_obs_counter < MOVE_OBS_MAX){
            // Only re-send obstacle
            subgoalsOnlySend(x, y, true);
        }
        else{
            // The trajectory is on an unknown obstacle, re-send it
            sendMainGoalAgain();
        }
        // Unlock the mutex
        n_obs_counter_mutex.unlock();
    }
    else{
        // Unlock the mutex
        subtrajectory_mutex.unlock();
#ifdef DEBUG_MODE
        cout << "STATE: trajectory on obstacle. GMP shall take care." << endl;
#endif
        std_msgs::Bool obs;
        obs.data = true;
        droneObsPub.publish(obs);
        // Lock mutex
        trajectory_set_mutex.lock();
        // Update state variable
        trajectory_set = false;
        // Unlock mutex
        trajectory_set_mutex.unlock();
    }
}

void DroneNavigationStackROSModule::droneTrajCallback(nav_msgs::Path::ConstPtr msg){
    if(moduleStarted){
        /// Lock callback, no other executions at the same time
        traj_callback_mutex.lock();
        /// Compute the new trajectory
        if(msg->poses.size() > 0){
            //Lock trajectory
            current_trajectory_mutex.lock();
            // Asign new trajectory
            current_trajectory = *msg;
            //Unlock trajectory
            current_trajectory_mutex.unlock();

            // Init final goal
            final_goal_mutex.lock();
            final_goal = false;
            final_goal_mutex.unlock();


            // Check if state should be cleaned up
            stateCleaner();

            // Check for sharp curvature
            /*if (!SharpCornersTrajectoryChecking()){*/
            if (1){

                // Initialize variables
                std_msgs::Bool goal;
                goal.data = false;
                droneGoalPub.publish(goal);
                droneObsPub.publish(goal);

                // Calculate length of the trajectory
                double trajectory_length = 0;
                double short_length;

                for(int i = 0; i < msg->poses.size(); i++){
#ifdef DEBUG_MODE
                    cout << "X: " << msg->poses[i].pose.position.x << "  Y: " << msg->poses[i].pose.position.y  << "  Z: " << msg->poses[i].pose.position.y << endl;
#endif
                    if (i < msg->poses.size() - 1){
                        short_length = sqrt(pow((msg->poses[i+1].pose.position.x - msg->poses[i].pose.position.x),2) + pow((msg->poses[i+1].pose.position.y - msg->poses[i].pose.position.y),2));
                        trajectory_length+=short_length;
                    }
                }

                following_index_mutex.lock();
                // Different distance beteween points depending on the status
                following_index_0 =  static_cast<int>( ((double) msg->poses.size() * (double) DIST_BET_POINTS_0) / (double) trajectory_length);
                following_index_1 =  static_cast<int>( ((double) msg->poses.size() * (double) DIST_BET_POINTS_1) / (double) trajectory_length);
                following_index_2 =  static_cast<int>( ((double) msg->poses.size() * (double) DIST_BET_POINTS_2) / (double) trajectory_length);


                following_index =  following_index_0;
                following_index_mutex.unlock();

#ifdef DEBUG_MODE
                cout << endl << endl;
                //            cout << "trajectory length: " << trajectory_length << "    following index: " << following_index << endl;
#endif
                //Lock trajectory
                current_trajectory_mutex.lock();
                // Set first point
                current_index = 0;
                current_pose = current_trajectory.poses[current_index];
                if (current_index + following_index < current_trajectory.poses.size()) following_pose = current_trajectory.poses[current_index + following_index];
                else    following_pose = current_trajectory.poses[current_index];
                //Unlock trajectory
                current_trajectory_mutex.unlock();

                // Set new angle
                double dot = following_pose.pose.position.x - current_pose.pose.position.x;      // dot product
                double det = following_pose.pose.position.y - current_pose.pose.position.y;     // determinant
                double angle = atan2(det, dot);  // atan2(y, x) or atan2(sin, cos)
                if (angle < 0)  angle = 2 * M_PI + angle;
#ifdef DEBUG_MODE
                //            cout << "full angle: " << angle << endl;
#endif

                // Last point should be precise
                if (current_index + following_index > current_trajectory.poses.size()){
                    current_pose = following_pose;
                }

                // Send point
                droneMsgsROS::dronePositionRefCommandStamped new_point;
                new_point.header.stamp = ros::Time::now();
                new_point.position_command.x = current_pose.pose.position.x;
                new_point.position_command.y = current_pose.pose.position.y;
                //new_point.position_command.z = ALTITUDE;
                new_point.position_command.z = last_goal_sent.z;
                dronePositionPub.publish(new_point);

#ifdef DEBUG_MODE
                //            cout << "New point: X: " << new_point.position_command.x << " Y: " << new_point.position_command.y << " Z: " << new_point.position_command.z << endl;
#endif

                //Lock trajectory
                current_trajectory_mutex.lock();
                previous_angle_mutex.lock();
                if (current_index + following_index >= current_trajectory.poses.size())	angle = previous_angle;
                //Unlock trajectory
                previous_angle_mutex.unlock();
                current_trajectory_mutex.unlock();

                // Select between the four options
                if((angle <= FORTY_FIVE_IN_RAD) || (angle > M_THREE_HALF_PI + FORTY_FIVE_IN_RAD)){
                    angle = 0;
                }
                else if((angle > M_HALF_PI - FORTY_FIVE_IN_RAD) && (angle <= M_HALF_PI + FORTY_FIVE_IN_RAD)){
                    angle = M_HALF_PI;
                }
                else if((angle > M_PI - FORTY_FIVE_IN_RAD) && (angle <= M_PI + FORTY_FIVE_IN_RAD)){
                    angle = M_PI;
                }
                else if((angle > M_THREE_HALF_PI - FORTY_FIVE_IN_RAD) && (angle <= M_THREE_HALF_PI + FORTY_FIVE_IN_RAD)){
                    angle = M_THREE_HALF_PI;
                }

                //            cout << "thresholded angle: " << angle << endl;

                // If requried, send the angle
                Yaw_mutex.lock();
                previous_angle_mutex.lock();
                if ((previous_angle != angle) && (!dont_yaw)){
                    Yaw.header.stamp = ros::Time::now();
                    Yaw.yaw = angle;
                    previous_angle = angle;
                    publishYaw(Yaw);
                    dont_move = true;
                    cout << "Turning in Yaw" << endl;
                }

                previous_angle_mutex.unlock();
                Yaw_mutex.unlock();
                // Lock the mutex
                trajectory_set_mutex.lock();
                // New trajectory is set
                trajectory_set = true;
                // Unlock the mutex
                trajectory_set_mutex.unlock();
            }
            else{
                // Send subgoal and update state variable
                subgoalsEraseFrontAndSend();
            }
        }
        else{   // Do something! The trajectory may be on an obstacle
            onObstacleSolver();
        }
        /// Unlock callback
        traj_callback_mutex.unlock();
    }
}

void DroneNavigationStackROSModule::droneOdomCallback(nav_msgs::Odometry::ConstPtr msg){
    if(moduleStarted){
        //    cout << "Received odometry" << endl;
        droneOdomPub.publish(*msg);

        /// Storing new yaw value
        ///
        tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
        tf::Matrix3x3 m(q);

        //convert quaternion to euler angels
        double y, p, r;
        m.getEulerYPR(y, p, r);
        current_yaw = y;
        if (current_yaw < 0)  current_yaw = 2 * M_PI + current_yaw;  //No angles lower < 0

        // Lock the Yaw
        Yaw_mutex.lock();
        // Check whether the angle is reached
        if ((current_yaw <  Yaw.yaw + ROT_TOLERANCE) && (current_yaw  > Yaw.yaw - ROT_TOLERANCE)){
            // Angle reached! Now move
            if (dont_move)  cout << "Now moving" << endl;
            dont_move = false;

            //cout << "Yaw reached!" << endl;

            //#ifdef DEBUG_MODE
            //            cout << "Yaw.yaw" << Yaw.yaw << endl;
            //#endif


            // Update variables if final goal
            final_goal_mutex.lock();
            if(final_goal && abs(last_goal_sent.z - msg->pose.pose.position.z) < TOL_IN_LAST_GOAL_Z	&& globalGoal){
                final_goal_mutex.unlock();
                // Tell GMP that you reached the goal
                std_msgs::Bool goal;
                goal.data = true;
                droneGoalPub.publish(goal);

//#ifdef DEBUG_MODE
                cout << "Goal reached!" << endl;
//#endif


            		// Only Global Mission planner can assign goals
            		globalGoal = false;
								goal_reached_counter = 0;

                // Enable final goal
                final_goal_mutex.lock();
                final_goal = false;
                final_goal_mutex.unlock();
            }
            else    final_goal_mutex.unlock();

        }
        // Unlock the variable
        Yaw_mutex.unlock();
        //    else{   // Updating again the Yaw
        //        Yaw.header.stamp = ros::Time::now();
        //        publishYaw(Yaw);
        //        if (ros::Time::now().sec - timeout.sec > YAW_TIMEOUT_SEC){
        //            // Timeout reached!
        //            cout << "timeout reached in yaw!" << endl;
        //            timeout = ros::Time::now();
        //            dont_move = false;
        //        }
        //    }
        // Lock mutex
        trajectory_set_mutex.lock();
        // Check whether the set point is reached
        if(trajectory_set && !dont_move){
            // Unlock mutex
            trajectory_set_mutex.unlock();
            if((msg->pose.pose.position.x < current_pose.pose.position.x + x_tolerance) &&
                    (msg->pose.pose.position.x > current_pose.pose.position.x - x_tolerance)){   //Inside in X
                if((msg->pose.pose.position.y < current_pose.pose.position.y + y_tolerance) &&
                        (msg->pose.pose.position.y > current_pose.pose.position.y - y_tolerance)){ //Inside in Y
#ifdef DEBUG_MODE
                    // Position reached!
                    cout << "Point reached!" << endl;
#endif
                    following_index_mutex.lock();
                    current_index+=following_index;
                    following_index_mutex.unlock();

                    // Update previous status
                    status_prev = status;

                    // If higher than the last point, go to last point
                    //                if ((current_index -  following_index < current_trajectory.poses.size() - 1)
                    //                        && (current_index >= current_trajectory.poses.size()) - 1)
                    //                                current_index = current_trajectory.poses.size() - 1;
                    //Lock trajectory
                    current_trajectory_mutex.lock();
                    if (current_index < current_trajectory.poses.size()){
                        // Set next point
                        previous_pose = current_pose;
                        current_pose = current_trajectory.poses[current_index];
                        following_index_mutex.lock();
                        if (current_index + following_index < current_trajectory.poses.size()) following_pose = current_trajectory.poses[current_index + following_index];
                        else    following_pose = current_trajectory.poses[current_trajectory.poses.size() - 1];
                        following_index_mutex.unlock();
                        //Unlock trajectory
                        current_trajectory_mutex.unlock();

                        // Set new angle
                        //                    double dot = odom_pose.pose.pose.position.x * current_pose.pose.position.x + odom_pose.pose.pose.position.y * current_pose.pose.position.y;      // dot product
                        //                    double det = odom_pose.pose.pose.position.x * current_pose.pose.position.y - odom_pose.pose.pose.position.y * current_pose.pose.position.x;     // determinant
                        double dot = following_pose.pose.position.x - current_pose.pose.position.x;      // dot product
                        double det = following_pose.pose.position.y - current_pose.pose.position.y;     // determinant
                        double angle = atan2(det, dot);  // atan2(y, x) or atan2(sin, cos)
                        if (angle < 0)  angle = 2 * M_PI + angle;
#ifdef DEBUG_MODE
                        cout << "full angle: " << angle << endl;
#endif

                        // Last point should be precise
                        if (current_index + following_index > current_trajectory.poses.size()){
                            current_pose = following_pose;
                        }

                        // Send point
                        droneMsgsROS::dronePositionRefCommandStamped new_point;
                        new_point.header.stamp = ros::Time::now();
                        new_point.position_command.x = current_pose.pose.position.x;
                        new_point.position_command.y = current_pose.pose.position.y;
                        //new_point.position_command.z = ALTITUDE;
                        new_point.position_command.z = last_goal_sent.z;
                        dronePositionPub.publish(new_point);

                        // Select between the four options
                        if((angle <= FORTY_FIVE_IN_RAD) || (angle > M_THREE_HALF_PI + FORTY_FIVE_IN_RAD)){
                            angle = 0;
                        }
                        else if((angle > M_HALF_PI - FORTY_FIVE_IN_RAD) && (angle <= M_HALF_PI + FORTY_FIVE_IN_RAD)){
                            angle = M_HALF_PI;
                        }
                        else if((angle > M_PI - FORTY_FIVE_IN_RAD) && (angle <= M_PI + FORTY_FIVE_IN_RAD)){
                            angle = M_PI;
                        }
                        else if((angle > M_THREE_HALF_PI - FORTY_FIVE_IN_RAD) && (angle <= M_THREE_HALF_PI + FORTY_FIVE_IN_RAD)){
                            angle = M_THREE_HALF_PI;
                        }

                        //Lock trajectory
                        current_trajectory_mutex.lock();
                        following_index_mutex.lock();
                        previous_angle_mutex.lock();
                        if (current_index + following_index >= current_trajectory.poses.size())	angle = previous_angle;
                        previous_angle_mutex.unlock();
                        following_index_mutex.unlock();
                        //Unlock trajectory
                        current_trajectory_mutex.unlock();

                        /// Checking if there is an obstacle close
                        ///
                        dont_yaw = false;
                        bool medium = false;
                        ranges_mutex.lock();
                        for(int i=0; i<ranges.size(); i++){
                            if(ranges[i] < CLOSE_DIST_TO_OBS){
                                if (status_prev == STATUS_MEDIUM){
                                    status = STATUS_CLOSE;
                                    x_tolerance = X_TOLERANCE_CLOSE;
                                    y_tolerance = Y_TOLERANCE_CLOSE;
                                    dont_yaw = true;
                                    dont_move = false;
                                    following_index_mutex.lock();
                                    following_index = following_index_2;
                                    following_index_mutex.unlock();
                                }
                                else{
                                    status = STATUS_MEDIUM;
                                    x_tolerance = X_TOLERANCE_MEDIUM;
                                    y_tolerance = Y_TOLERANCE_MEDIUM;
                                    following_index_mutex.lock();
                                    following_index = following_index_1;
                                    following_index_mutex.unlock();
                                    // Debug
                                    dont_yaw = false;
                                    dont_move = false;
                                }
                                break;
                            }
                            else if(ranges[i] < MEDIUM_DIST_TO_OBS){
                                status = STATUS_MEDIUM;
                                x_tolerance = X_TOLERANCE_MEDIUM;
                                y_tolerance = Y_TOLERANCE_MEDIUM;
                                medium = true;
                                following_index_mutex.lock();
                                following_index = following_index_1;
                                following_index_mutex.unlock();
                                // Debug
                                dont_yaw = false;
                                dont_move = false;
                            }
                            else{
                                if(!medium){
                                    status = STATUS_FAR;
                                    x_tolerance = X_TOLERANCE_FAR;
                                    y_tolerance = Y_TOLERANCE_FAR;
                                    following_index_mutex.lock();
                                    following_index = following_index_0;
                                    following_index_mutex.unlock();
                                    dont_yaw = false;
                                }
                            }
                        }
                        ranges_mutex.unlock();

                        // Last point should be precise
                        if (current_index + following_index > current_trajectory.poses.size()){
                            x_tolerance = TOL_IN_LAST_GOAL;
                            y_tolerance = TOL_IN_LAST_GOAL;
                        }

                        // Avoid keeping on yawing if within the state is not allowed
                        final_goal_mutex.lock();
                        if(dont_yaw && !final_goal){
                            // Publish the same yaw
                            droneMsgsROS::droneYawRefCommand yaw_stop;
                            yaw_stop.header.stamp = ros::Time::now();
                            yaw_stop.yaw = current_yaw;
                            publishYaw(yaw_stop);
                        }
                        final_goal_mutex.unlock();

#ifdef DEBUG_MODE
                        //                    cout << "thresholded angle: " << angle << endl;
                        //                    cout << "current angle: " << current_yaw << endl;
                        cout << "STATUS: " << status << endl;
                        following_index_mutex.lock();
                        cout << "following index: " << following_index << endl;
                        following_index_mutex.unlock();
                        if (status == STATUS_CLOSE){
                            cout << "distance between points: " << DIST_BET_POINTS_2 << " m" << endl;
                            cout << "torelance in points: " << X_TOLERANCE_CLOSE << " m" << endl;
                        }
                        else if (status == STATUS_MEDIUM){
                            cout << "distance between points: " << DIST_BET_POINTS_1 << " m" << endl;
                            cout << "torelance in points: " << X_TOLERANCE_MEDIUM << " m" << endl;
                        }
                        else{
                            cout << "distance between points: " << DIST_BET_POINTS_0 << " m" << endl;
                            cout << "torelance in points: " << X_TOLERANCE_FAR << " m" << endl;
                        }
#endif

                        // If requried, send the angle
                        Yaw_mutex.lock();
                        previous_angle_mutex.lock();
                        if ((previous_angle != angle) && (!dont_yaw)){
                            Yaw.header.stamp = ros::Time::now();
                            Yaw.yaw = angle;
                            previous_angle = angle;
                            publishYaw(Yaw);
                            dont_move = true;
#ifdef DEBUG_MODE
                            cout << "Turning in Yaw" << endl;
#endif
                        }
                        previous_angle_mutex.unlock();
                        Yaw_mutex.unlock();
                    }
                    else    {
                        //Unlock trajectory
                        current_trajectory_mutex.unlock();
                        // Lock mutex
                        trajectory_set_mutex.lock();
                        //Lock trajectory
                        current_trajectory_mutex.lock();
                        // Trajectory aborted, maybe because the goal is on an obstacle
                        if (trajectory_set && (current_index > current_trajectory.poses.size() - 1) ){
                            // Unlock mutex
                            trajectory_set_mutex.unlock();
                            //Unlock trajectory
                            current_trajectory_mutex.unlock();
                            // Lock state variable
                            subtrajectory_mutex.lock();
                            if(subtrajectory){
                                // Unlock state variable
                                subtrajectory_mutex.unlock();
                                // Send subgoal and update state variable
                                subgoalsEraseFrontAndSend();
                            }
                            else{
                                // Unlock state variable
                                subtrajectory_mutex.unlock();
                                last_goal_sent_mutex.lock();
#ifdef DEBUG_MODE
                                cout << "last_goal_sent.x " << last_goal_sent.x << "   msg->pose.pose.position.x   "  << msg->pose.pose.position.x << endl;
                                cout << "last_goal_sent.y " << last_goal_sent.y << "   msg->pose.pose.position.y   "  << msg->pose.pose.position.y << endl;
                                cout << "last_goal_sent.z " << last_goal_sent.z << "   msg->pose.pose.position.z   "  << msg->pose.pose.position.z << endl;
#endif
																
																goal_reached_counter++;

																// If goal reached missed, resend it
																if (goal_reached_counter > NO_GOAL_REACHED){
																		goal_reached_counter = 0;
																		globalGoal = true;																		
																}																
					
                                if(abs(last_goal_sent.x - msg->pose.pose.position.x) < TOL_IN_LAST_GOAL
                                        && abs(last_goal_sent.y - msg->pose.pose.position.y) < TOL_IN_LAST_GOAL){
                                    // Unlock the mutex
                                    last_goal_sent_mutex.unlock();

                                    if (last_goal_with_yaw.pose.orientation.x == 0 &&
                                            last_goal_with_yaw.pose.orientation.y == 0 &&
                                            last_goal_with_yaw.pose.orientation.z == 0 &&
                                            last_goal_with_yaw.pose.orientation.w == 1){
                                        // Disable final goal
                                        final_goal_mutex.lock();
                                        final_goal = true;
                                        final_goal_mutex.unlock();

                                        // Publish the same yaw
                                        droneMsgsROS::droneYawRefCommand yaw_stop;
                                        yaw_stop.header.stamp = ros::Time::now();
                                        yaw_stop.yaw = current_yaw;
                                        publishYaw(yaw_stop);

                                        // Yaw set is the current yaw
                                        Yaw_mutex.lock();
                                        Yaw.yaw = yaw_stop.yaw;
                                        Yaw_mutex.unlock();

#ifdef DEBUG_MODE
                                        cout << "Disabled final yaw " << endl;
#endif
                                    }
                                    else{   // Final goal and yaw
                                        // Send yaw position
                                        /// Storing new yaw value
                                        ///
                                        tf::Quaternion q(last_goal_with_yaw.pose.orientation.x, last_goal_with_yaw.pose.orientation.y, last_goal_with_yaw.pose.orientation.z, last_goal_with_yaw.pose.orientation.w);
                                        tf::Matrix3x3 m(q);

                                        //convert quaternion to euler angels
                                        double y, p, r;
                                        m.getEulerYPR(y, p, r);
                                        //                                if (y < 0)  y = 2 * M_PI + y;  //No angles lower < 0

                                        // Send the current yaw
                                        droneMsgsROS::droneYawRefCommand final_yaw;
                                        final_yaw.header.stamp = ros::Time::now();
                                        final_yaw.yaw = y;
                                        publishYaw(final_yaw);

                                        // Update yaw reference
                                        Yaw_mutex.lock();
                                        Yaw.yaw = y;
                                        if(Yaw.yaw < 0) Yaw.yaw += 2*M_PI;
                                        Yaw_mutex.unlock();

                                        // Enable final goal
                                        final_goal_mutex.lock();
                                        final_goal = true;
                                        final_goal_mutex.unlock();

#ifdef DEBUG_MODE
                                        cout << "Final yaw: " << final_yaw << endl;
#endif
                                    }
                                }
                                else{
                                    // Reset state
                                    subgoalsClear();
                                    // Send last goal
                                    sendGoal(last_goal_sent.x, last_goal_sent.y, false);
                                    // Unlock the mutex
                                    last_goal_sent_mutex.unlock();
                                }
                            }
                        }
                        else{
                            // Unlock mutex
                            trajectory_set_mutex.unlock();
                            //Unlock trajectory
                            current_trajectory_mutex.unlock();
                        }
                        //                    else if (trajectory_set){
                        //                        onObstacleSolver();
                        //                    }
                        // Lock mutex
                        trajectory_set_mutex.lock();
                        // Update state variable
                        trajectory_set = false;
                        // Unlock mutex
                        trajectory_set_mutex.unlock();
                    }
                }
            }
        }
        else{
            // Unlock mutex
            trajectory_set_mutex.unlock();
        }
    }
}

void DroneNavigationStackROSModule::droneScanCallback(sensor_msgs::LaserScan::ConstPtr msg){
    if(moduleStarted){
        ranges_mutex.lock();
        ranges.clear();
        for(int i=0; i<msg->ranges.size(); i++){
            ranges.push_back(msg->ranges[i]);
        }
        ranges_mutex.unlock();
    }
}

void DroneNavigationStackROSModule::droneRotCallback(geometry_msgs::Vector3Stamped::ConstPtr msg){
    //    rotation_angles = *msg;

    ////    cout << rotation_angles.vector.z << endl << endl;

    //    // Converting to radians
    //    rotation_angles.vector.z = rotation_angles.vector.z * M_PI / 180;

    //    // Adjusting angles to fit our calculations
    //    if (rotation_angles.vector.z < 0)  rotation_angles.vector.z = 2 * M_PI + rotation_angles.vector.z;  //No angles lower < 0
    ////    rotation_angles.vector.z= 2 * M_PI - rotation_angles.vector.z;




}

void DroneNavigationStackROSModule::close()
{
    DroneModule::close();



    //Do stuff

    return;
}


bool DroneNavigationStackROSModule::resetValues()
{
    //Do stuff
    // Lock mutex
    trajectory_set_mutex.lock();
    // Update state variable
    trajectory_set = false;
    // Unlock mutex
    trajectory_set_mutex.unlock();
    return true;
}


bool DroneNavigationStackROSModule::startVal()
{
    //Do stuff
    // Lock mutex
    trajectory_set_mutex.lock();
    // Update state variable
    trajectory_set = false;
    // Unlock mutex
    trajectory_set_mutex.unlock();

    //End
    return DroneModule::startVal();
}


bool DroneNavigationStackROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool DroneNavigationStackROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}






